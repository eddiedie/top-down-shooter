// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/AudioComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "NavigationPath.h"
#include "NavigationSystem.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "TDS/SprintPoint.h"
#include "Engine/World.h"


ATDSCharacter::ATDSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;



	// Create Trigger Capsule
	TriggerCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Trigger Capsule"));
	TriggerCapsule->InitCapsuleSize(42.f, 96.0f);
	TriggerCapsule->SetCollisionProfileName(TEXT("Trigger"));
	TriggerCapsule->SetupAttachment(RootComponent);

	TriggerCapsule->OnComponentBeginOverlap.AddDynamic(this,&ATDSCharacter::OnOverlapBegin);
	TriggerCapsule->OnComponentEndOverlap.AddDynamic(this,&ATDSCharacter::OnOverlapEnd);


	// Create SprintPoint
	SprintPoint = CreateDefaultSubobject<ASprintPoint>(TEXT("Sprint Point"));
	SprintPoint->SetActorScale3D(FVector(1.5,1.5,1.5));


	// Create SplineComponent
	SplineComponent = CreateDefaultSubobject<USplineComponent>(TEXT("mySpline"));
	SplineComponent->SetDrawDebug(true);
	AudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComponent"));
	AudioComponent->SetupAttachment(RootComponent);
	AudioComponent->bAutoActivate = false;
}



void ATDSCharacter::BeginPlay()
{
	Super::BeginPlay();
	APlayerController* PlayerController = Cast<APlayerController>(UGameplayStatics::GetPlayerController(GetWorld(),0));
	if (PlayerController)
	{
		UKismetSystemLibrary::ExecuteConsoleCommand(GetWorld(),"Show Splines",PlayerController);
	}
	SplineComponent->SetDrawDebug(true);
	
}



void ATDSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}
	MovementTick(DeltaSeconds);
}

void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveForward",this,&ATDSCharacter::InputAxisX);
	PlayerInputComponent->BindAxis("MoveRight",this,&ATDSCharacter::InputAxisY);
	PlayerInputComponent->BindAction("AddSprintPoints", IE_Pressed,this,&ATDSCharacter::AddSprintPoints);
	PlayerInputComponent->BindAction("GoToSprintPoints", IE_Pressed,this,&ATDSCharacter::GoToSprintPoints);
	PlayerInputComponent->BindAction("RemoveSprintPoints", IE_Pressed,this,&ATDSCharacter::RemoveSprintPoints);
	PlayerInputComponent->BindAction("RemoveAllSprintPoints", IE_DoubleClick,this,&ATDSCharacter::RemoveAllSprintPoints);
}

void ATDSCharacter::InputAxisX(float value)
{
	AxisX = value;
}
void ATDSCharacter::InputAxisY(float value)
{
	AxisY = value;
}

void ATDSCharacter::MovementTick(float DeltaTime)
{
	AddMovementInput(FVector(1,0,0),AxisX);
	AddMovementInput(FVector(0,1,0),AxisY);

	APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(),0);
	if (PlayerController)
	{
		if (EnableRotation)
		{
			FHitResult HitResult;
			//PlayerController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6,false,HitResult);
			PlayerController->GetHitResultUnderCursor(ECC_GameTraceChannel1,true,HitResult);
			float NewRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(),HitResult.Location).Yaw;
			SetActorRotation(FRotator(0,NewRotation,0));
		}
	}
}

void ATDSCharacter::CharacterUpdate()
{
	float Speed = 600;
	switch (MovementState)
	{
	case EMovementState::Run_State:
		Speed = SpeedInfo.RunSpeed;
		break;

	case EMovementState::Walk_State:
		Speed = SpeedInfo.WalkSpeed;
		break;

	case EMovementState::AimRun_State:
		Speed = SpeedInfo.AimRunSpeed;
		break;

	case EMovementState::AimWalk_State:
		Speed = SpeedInfo.AimWalkSpeed;
		break;

	case EMovementState::Sprint_State:
		Speed = SpeedInfo.SprintSpeed;
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = Speed;
}

void ATDSCharacter::ChangeMovementState()
{
	if (!SprintEnabled && !AimEnabled && !WalkEnabled)
	{
		MovementState = EMovementState::Run_State;
	}

	if (AimEnabled && !WalkEnabled && !SprintEnabled)
	{
		MovementState = EMovementState::AimRun_State;
	}

	if (WalkEnabled && !AimEnabled)
	{
		SprintEnabled = false;
		MovementState = EMovementState::Walk_State;
	}

	if (WalkEnabled && AimEnabled)
	{
		SprintEnabled = false;
		MovementState = EMovementState::AimWalk_State;
	}

	if (SprintEnabled)
	{
		AimEnabled = false;
		WalkEnabled = false;
		MovementState = EMovementState::Sprint_State;
	}

	CharacterUpdate();
}




/*------------------------------- Sprint to Cursor ------------------------------------*/


void ATDSCharacter::AddSprintPoints()
{
	auto SplinePath = UNavigationSystemV1::FindPathToLocationSynchronously(GetWorld(),GetActorLocation(),CursorToWorld->GetComponentLocation());
	
	auto SplineArray = SplinePath->PathPoints;
		
	SplineComponent->ClearSplinePoints(true);
	
	
	for (auto Array : SplinePath->PathPoints)
	{
		SplineComponent->AddSplinePoint(Array,ESplineCoordinateSpace::World,true);
		SplineComponent->AddSplinePointAtIndex(Array,0,ESplineCoordinateSpace::World);
	}
	auto PointsNum = SplineComponent->GetNumberOfSplinePoints();
	auto PointPos = SplineComponent->GetLocationAtSplinePoint(PointsNum,ESplineCoordinateSpace::World);
	

	auto CursorPos = CursorToWorld->GetComponentLocation();
	

	if (FVector::PointsAreNear(CursorPos,PointPos,50))
	{
		if (FVector::PointsAreNear(PointPos,GetActorLocation(),150))
		{
			GEngine->AddOnScreenDebugMessage(+1,2,FColor::Orange,"Can't create SprintPoint");
			AudioComponent->SetIntParameter("PickUp",1);
			AudioComponent->Play();
		}
		else
		{
			GEngine->AddOnScreenDebugMessage(5,2,FColor::Red,"Equal");
			if (ClickNum > 0)
			{
				SprintPoint = GetWorld()->SpawnActor<ASprintPoint>(SprintPointClass,FTransform(CursorPos));
				SprintArray.Add(SprintPoint);
				SprintPoint->SprintOwner = this;
				auto SprintArrayNum = SprintArray.Num();
				ClickNum -= 1;
				GEngine->AddOnScreenDebugMessage(2,2,FColor::Orange,FString::Printf(TEXT("Number of created points: %d"),SprintArrayNum));
				GEngine->AddOnScreenDebugMessage(1,2,FColor::Orange,FString::Printf(TEXT("Points to create left: %d"),ClickNum));
				GEngine->AddOnScreenDebugMessage(0,2,FColor::Orange,"Create SprintPoint");
			}
			else
			{
				AudioComponent->SetIntParameter("PickUp",1);
				AudioComponent->Play();
			}
		}
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(5,2,FColor::Red,"Not Equal");
		AudioComponent->SetIntParameter("PickUp",1);
		AudioComponent->Play();
	}
	
}





void ATDSCharacter::RemoveSprintPoints()
{
	if (SprintArray.Num() != 0)
	{
		APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(),0);
		if (PlayerController)
		{
			FHitResult HitResult;
			
			PlayerController->GetHitResultUnderCursor(ECC_GameTraceChannel2,true,HitResult);
			//PlayerController->GetHitResultUnderCursorForObjects(ObjectType, true,HitResult);
			ASprintPoint* SprintPt = Cast<ASprintPoint>(HitResult.GetActor());
			if (SprintPt)
			{
				GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Green, FString::Printf(TEXT("I Hit: %s"), *SprintPt->GetName()));
				int32 Index;
				SprintArray.Find(SprintPt,Index);
				GEngine->AddOnScreenDebugMessage(1,5,FColor::Blue,FString::Printf(TEXT("It's number is %d"),Index));
				SprintPt->Destroy();
				SprintArray.RemoveAt(Index,1,true);
				ClickNum +=1;
				AudioComponent->SetIntParameter("PickUp",0);
				AudioComponent->Play();
			}
			
			
		}
	}
}



void ATDSCharacter::RemoveAllSprintPoints()
{
	if (SprintArray.Num() != 0)
	{
		for (int i = 0; i < SprintArray.Num(); i++)
		{
			SprintArray[i]->Destroy();
			ClickNum +=1;
			AudioComponent->SetIntParameter("PickUp",0);
			AudioComponent->Play();
		}
		SprintArray.Empty();
	}
}



void ATDSCharacter::GoToSprintPoints()
{
	if (SprintArray.Num() > 0)
	{
		float const Distance = FVector::Dist(SprintArray[0]->GetActorLocation(), GetActorLocation());
	
		if ((Distance > 120.0f))
		{
			APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(),0);
			if (PlayerController)
			{
				UAIBlueprintHelperLibrary::SimpleMoveToLocation(PlayerController, SprintArray[0]->GetActorLocation());
			}
		}
		SprintEnabled = true;
		EnableRotation = false;
		ChangeMovementState();
		APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(),0);
		if (PlayerController)
		{
			UAIBlueprintHelperLibrary::SimpleMoveToLocation(PlayerController, SprintArray[0]->GetActorLocation());
			DisableInput(PlayerController);
		}
	}

}





// void ATDSCharacter::SprintToCursor()
// {
// 	auto CursorPos = CursorToWorld->GetComponentLocation();
// 	
// 	GetWorld()->SpawnActor<ASprintPoint>(SprintPointClass,FTransform(CursorPos));
// 	
// 	float const Distance = FVector::Dist(CursorPos, GetActorLocation());
// 	
//
// 	if ((Distance > 120.0f))
// 	{
// 		APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(),0);
// 		if (PlayerController)
// 		{
// 			UAIBlueprintHelperLibrary::SimpleMoveToLocation(PlayerController, CursorPos);
// 			//DisableInput(PlayerController);
// 		}
// 	}
// 	SprintEnabled = true;
// 	EnableRotation = false;
// 	ChangeMovementState();
// }


void ATDSCharacter::OnOverlapBegin
			(
				UPrimitiveComponent*		OverlappedComponent,
				AActor*						OtherActor,
				UPrimitiveComponent*		OtherComponent,
				int32						OtherBodyIndex,
				bool						bFromSweep,
				const FHitResult&			SweepResult
			)
{
	
	
	

	ASprintPoint* SprintPt = Cast<ASprintPoint>(OtherActor);
	if (SprintPt && SprintPt == SprintArray[0])
	{
		GEngine->AddOnScreenDebugMessage(-1,5,FColor::Red,"Overlap Begin!");
		
		SprintPt->Destroy();
				
		SprintArray.RemoveAt(0,1,true);
		AudioComponent->SetIntParameter("PickUp",2);
		AudioComponent->Play();
		GoToSprintPoints();
		if (SprintArray.Num() == 0)
		{
			APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(),0);
			if (PlayerController)
			{
				EnableRotation = true;
				SprintEnabled = false;
				ChangeMovementState();
				EnableInput(PlayerController);
				ClickNum = 5;
			}
		}
	}

	
}

void ATDSCharacter::OnOverlapEnd
			(
				UPrimitiveComponent*		OverlappedComponent,
				AActor*						OtherActor,
				UPrimitiveComponent*		OtherComponent,
				int32						OtherBodyIndex
			)
{
	ASprintPoint* SprintPt = Cast<ASprintPoint>(OtherActor);
	if (SprintPt)
	{
		GEngine->AddOnScreenDebugMessage(-1,5,FColor::Red,"Overlap End!");
	}
	
}



void ATDSCharacter::CreateSpline()
{
	auto SplinePath = UNavigationSystemV1::FindPathToLocationSynchronously(GetWorld(),GetActorLocation(),CursorToWorld->GetComponentLocation());
	
	auto SplineArray = SplinePath->PathPoints;
		
	SplineComponent->ClearSplinePoints(true);
	
	
	for (auto Array : SplinePath->PathPoints)
	{
		SplineComponent->AddSplinePoint(Array,ESplineCoordinateSpace::World,true);
		
	}
	auto PointsNum = SplineComponent->GetNumberOfSplinePoints();
	auto PointPos = SplineComponent->GetLocationAtSplinePoint(PointsNum,ESplineCoordinateSpace::World);
	

	auto CursorPos = CursorToWorld->GetComponentLocation();
	

	if (FVector::PointsAreNear(CursorPos,PointPos,50))
	{
		GEngine->AddOnScreenDebugMessage(5,2,FColor::Red,"Equal");
		
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(5,2,FColor::Red,"Not Equal");
	}
	
}
