// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SprintPoint.generated.h"


class UStaticMeshComponent;
class ATDSCharacter;

UCLASS()
class TDS_API ASprintPoint : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASprintPoint();

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	UStaticMeshComponent* PointMeshComp;

	UPROPERTY()
	ATDSCharacter* SprintOwner;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	// UFUNCTION()
	// 	void HandleBeginOverlap
	// 		(
	// 			UPrimitiveComponent*		OverlappedComponent,
	// 			AActor*						OtherActor,
	// 			UPrimitiveComponent*		OtherComponent,
	// 			int32						OtherBodyIndex,
	// 			bool						bFromSweep,
	// 			const FHitResult&			SweepResult
	// 		);

};
