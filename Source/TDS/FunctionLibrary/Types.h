// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Types.generated.h"

/**
 * 
 */
 UCLASS()
class TDS_API UTypes : public UBlueprintFunctionLibrary
 {
 	GENERATED_BODY()
 };

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	AimRun_State UMETA(DisplayName = "AimRun State"),
	AimWalk_State UMETA(DisplayName = "AimWalk State"),
	Walk_State UMETA(DisplayName = "Walk State"),
	Run_State UMETA(DisplayName = "Run State"),
	Sprint_State UMETA(DisplayName = "Sprint State")
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Movement")
	float AimRunSpeed = 200;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Movement")
	float AimWalkSpeed = 75;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Movement")
	float WalkSpeed = 200;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Movement")
	float RunSpeed = 600;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Movement")
	float SprintSpeed = 1000;
 
};
